#! /bin/bash

set -e

url=$1

vid_info=`ytdl "$url" -i`

# this script uses ytdl (node.js cli tool) to fetch youtube 1080p videos and their separate sound files and merges them into one high quality video.

# todo: either replace this with a node-script, use curl instead of ytdl, or open a PR to make ytdl do this OOTB.

# 1. extract title, author, and audio + video tags
# 2. download the audio and video channels (this script only downloads 1080p)
# 3. merge the two via ffmpeg

# 1. extract title, author, and audio + video tags
video_title=`echo "$vid_info" | grep -E "title" | cut -d ":" -sf2`
# echo video_title: $video_title

video_author=`echo "$vid_info" | grep -E "author" | cut -d ":" -sf2`
# echo video_author: $video_author

video_tag=`echo "$vid_info" | grep -E "mp4\s+1080p" | cut -d" " -sf2`
# echo video_tag: $video_tag

audio_tag=`echo "$vid_info" | grep -E "mp4\s+aac" | cut -d" " -sf2`
# echo audio_tag: $audio_tag

# 2. download the audio and video channels (this script only downloads 1080p)
ytdl "$url" -q $video_tag > video.mp4
ytdl "$url" -q $audio_tag > audio.mp4

# 3. merge the two via ffmpeg
ffmpeg -i video.mp4 -i audio.mp4 -c:v copy -c:a aac "$video_author - $video_title.mp4"

